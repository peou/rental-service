import Sequelize from 'sequelize';
import logger from '../config/logger';

import dotenv from 'dotenv';
dotenv.config();

export { DataTypes } from 'sequelize';

let DATABASE = process.env.POSTGRES_DB;
let USERNAME = process.env.POSTGRES_USER;
let PASSWORD = process.env.POSTGRES_PASSWORD;
let HOST = process.env.POSTGRES_HOST;
let PORT = process.env.POSTGRES_PORT;
let DIALECT = process.env.DIALECT;

const sequelize = new Sequelize(DATABASE, USERNAME, PASSWORD, {
  host: HOST,
  port: PORT,
  dialect: DIALECT,
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
});

sequelize
  .authenticate()
  .then(() => {
    logger.info('Connected to the database.');
  })
  .catch((error) => {
    logger.error('Could not connect to the database.', error);
  });

sequelize.sync();

export default sequelize;
