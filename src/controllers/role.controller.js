import HttpStatus from 'http-status-codes';
import * as RoleService from '../services/role.service';

/**
 * Controller to get all roles available
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const getAllRoles = async (req, res, next) => {
  try {
    const data = await RoleService.getAllRoles();
    let filteredArticles = data;
    const { page = 1, size = 10 } = req.query;
    const pageNum = parseInt(page, 10);
    const pageSize = parseInt(size, 10);
    const startIndex = (pageNum - 1) * pageSize;
    const endIndex = startIndex + pageSize;

    // Get the subset of articles for the current page
    const paginatedArticles = filteredArticles.slice(startIndex, endIndex);

    // Prepare the pagination response
    const totalArticles = filteredArticles.length;
    const totalPages = Math.ceil(totalArticles / pageSize);

    res.status(HttpStatus.OK).json({
      
      code: HttpStatus.OK,
      page: pageNum,
      size: pageSize,
      totalPages,
      totalArticles,
      data: paginatedArticles,
      message: 'All roles fetched successfully'
    });
  } catch (error) {
    next(error);
  }
};

 /**
 * Controller to get a single role
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
 export const getRole = async (req, res, next) => {
  try {
    const role = await RoleService.getRole(req.params.id);
    if (role) {
      res.status(HttpStatus.OK).json({
        code: HttpStatus.OK,
        role: role,
        message: 'Role fetched successfully'
      });
    } else {
      res.status(HttpStatus.NOT_FOUND).json({
        code: HttpStatus.NOT_FOUND,
        message: 'Role does not exist'
      });
    }
  } catch (error) {
    next(error);
  }
};

 /**
 * Controller to get a single role
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
 export const getRoleBySearch = async (req, res, next) => {
  try {
    const { value } = req.query; // Assuming you're searching by name
  
    const roleData = await RoleService.getRoleBySearch(value);

    let filteredArticles = roleData;
    const { page = 1, size = 10 } = req.query;
    const pageNum = parseInt(page, 10);
    const pageSize = parseInt(size, 10);
    const startIndex = (pageNum - 1) * pageSize;
    const endIndex = startIndex + pageSize;

    // Get the subset of articles for the current page
    const paginatedArticles = filteredArticles.slice(startIndex, endIndex);

    // Prepare the pagination response
    const totalArticles = filteredArticles.length;
    const totalPages = Math.ceil(totalArticles / pageSize);
    
    // If found, send the data in the response
    if(totalArticles == 0){
      res.status(HttpStatus.OK).json({
        code: HttpStatus.OK,
        message: 'Searching not found'
      });
    }else{
      res.status(HttpStatus.OK).json({
        code: HttpStatus.OK,
        page: pageNum,
        size: pageSize,
        totalPages,
        totalArticles,
        data: paginatedArticles,
        message: 'searching successfully'
      });
    }
  } catch (error) {
    // If an error occurs, pass it to the error handling middleware
    next(error);
  }
}


/**
 * Controller to create a new role
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const newRole = async (req, res, next) => {
  try {
    const data = await RoleService.newRole(req.body);
    res.status(HttpStatus.CREATED).json({
      code: HttpStatus.CREATED,
      data: data,
      message: 'role created successfully'
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Controller to update a role
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const updateRole = async (req, res, next) => {
  try {
    const data = await RoleService.updateRole(req.params.id, req.body);
    res.status(HttpStatus.ACCEPTED).json({
      code: HttpStatus.ACCEPTED,
      data: data,
      message: 'Role updated successfully'
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Controller to delete a single role
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const deleteRole = async (req, res, next) => {
  try {
    await RoleService.deleteRole(req.params.id);
    res.status(HttpStatus.OK).json({
      code: HttpStatus.OK,
      data: [],
      message: 'Role deleted successfully'
    });
  } catch (error) {
    next(error);
  }
};
