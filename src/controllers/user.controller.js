import HttpStatus from 'http-status-codes';
import * as UserService from '../services/user.service';

/**
 * Controller to get all users available
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const getAllUsers = async (req, res, next) => {
  try {
    const data = await UserService.getAllUsers();
    let filteredArticles = data;
    const { page = 1, size = 10 } = req.query;
    const pageNum = parseInt(page, 10);
    const pageSize = parseInt(size, 10);
    const startIndex = (pageNum - 1) * pageSize;
    const endIndex = startIndex + pageSize;

    // Get the subset of articles for the current page
    const paginatedArticles = filteredArticles.slice(startIndex, endIndex);

    // Prepare the pagination response
    const totalArticles = filteredArticles.length;
    const totalPages = Math.ceil(totalArticles / pageSize);

    res.status(HttpStatus.OK).json({
      
      code: HttpStatus.OK,
      page: pageNum,
      size: pageSize,
      totalPages,
      totalArticles,
      data: paginatedArticles,
      message: 'All users fetched successfully'
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Controller to get a single user
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const getUser = async (req, res, next) => {
  try {
    const user = await UserService.getUser(req.params.id);
    if (user) {
      res.status(HttpStatus.OK).json({
        code: HttpStatus.OK,
        user: user,
        message: 'User fetched successfully'
      });
    } else {
      res.status(HttpStatus.NOT_FOUND).json({
        code: HttpStatus.NOT_FOUND,
        message: 'User does not exist'
      });
    }
  } catch (error) {
    next(error);
  }
};

/**
* Controller to get a single role
* @param  {object} req - request object
* @param {object} res - response object
* @param {Function} next
*/
export const getUserBySearch = async (req, res, next) => {
 try {
   const { value } = req.query; // Assuming you're searching by name
   const userData = await UserService.getUserBySearch(value);
   let filteredArticles = userData;
   const { page = 1, size = 10 } = req.query;
   const pageNum = parseInt(page, 10);
   const pageSize = parseInt(size, 10);
   const startIndex = (pageNum - 1) * pageSize;
   const endIndex = startIndex + pageSize;

   // Get the subset of articles for the current page
   const paginatedArticles = filteredArticles.slice(startIndex, endIndex);

   // Prepare the pagination response
   const totalArticles = filteredArticles.length;
   const totalPages = Math.ceil(totalArticles / pageSize);
   
   if(totalArticles==0){
     res.status(HttpStatus.OK).json({
       code: HttpStatus.OK,
       message: 'Searching not found'
     });
   }else{
     res.status(HttpStatus.OK).json({
       code: HttpStatus.OK,
       page: pageNum,
       size: pageSize,
       totalPages,
       totalArticles,
       data: paginatedArticles,
       message: 'searching successfully'
     });
   }
 } catch (error) {
   // If an error occurs, pass it to the error handling middleware
   next(error);
 }
}

/**
 * Controller to create a new user
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const newUser = async (req, res, next) => {
  try {
    const data = await UserService.newUser(req.body);
    res.status(HttpStatus.CREATED).json({
      code: HttpStatus.CREATED,
      data: data,
      message: 'user created successfully'
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Controller to update a user
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const updateUser = async (req, res, next) => {
  try {
    const data = await UserService.updateUser(req.params.id, req.body);
    res.status(HttpStatus.ACCEPTED).json({
      code: HttpStatus.ACCEPTED,
      data: data,
      message: 'User updated successfully'
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Controller to delete a single user
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const deleteUser = async (req, res, next) => {
  try {
    await UserService.deleteUser(req.params.id);
    res.status(HttpStatus.OK).json({
      code: HttpStatus.OK,
      data: [],
      message: 'User deleted successfully'
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Controller to delete a single user
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const login = async (req, res, next) => {
  try {
    const user = await UserService.login(req.body);
    res.status(HttpStatus.OK).json({
      code: HttpStatus.OK,
      data: { user: user.dataValues },
      message: 'Login successfully'
    });
  } catch (error) {
    next(error);
  }
};
