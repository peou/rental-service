import HttpStatus from 'http-status-codes';
import * as PostService from '../services/post.service';

/**
 * Controller to get all Post available
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const getAllPosts = async (req, res, next) => {
  try {
    const data = await PostService.getAllPosts();
    let filteredArticles = data;
    const { page = 1, size = 10 } = req.query;
    const pageNum = parseInt(page, 10);
    const pageSize = parseInt(size, 10);
    const startIndex = (pageNum - 1) * pageSize;
    const endIndex = startIndex + pageSize;

    // Get the subset of articles for the current page
    const paginatedArticles = filteredArticles.slice(startIndex, endIndex);

    // Prepare the pagination response
    const totalArticles = filteredArticles.length;
    const totalPages = Math.ceil(totalArticles / pageSize);

    res.status(HttpStatus.OK).json({
      
      code: HttpStatus.OK,
      page: pageNum,
      size: pageSize,
      totalPages,
      totalArticles,
      data: paginatedArticles,
      message: 'All Posts fetched successfully'
    });
  } catch (error) {
    next(error);
  }
};

 /**
 * Controller to get a single role
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
 export const getPostBySearch = async (req, res, next) => {
  try {
    const { value } = req.query; // Assuming you're searching by name
    const postData = await PostService.getPostBySearch(value);
    let filteredArticles = postData;
    const { page = 1, size = 10 } = req.query;
    const pageNum = parseInt(page, 10);
    const pageSize = parseInt(size, 10);
    const startIndex = (pageNum - 1) * pageSize;
    const endIndex = startIndex + pageSize;

    // Get the subset of articles for the current page
    const paginatedArticles = filteredArticles.slice(startIndex, endIndex);

    // Prepare the pagination response
    const totalArticles = filteredArticles.length;
    const totalPages = Math.ceil(totalArticles / pageSize);
    
    if(totalArticles==0){
      res.status(HttpStatus.OK).json({
        code: HttpStatus.OK,
        message: 'Searching not found'
      });
    }else{
      res.status(HttpStatus.OK).json({
        code: HttpStatus.OK,
        page: pageNum,
        size: pageSize,
        totalPages,
        totalArticles,
        data: paginatedArticles,
        message: 'searching successfully'
      });
    }
  } catch (error) {
    // If an error occurs, pass it to the error handling middleware
    next(error);
  }
}

 /**
 * Controller to get a single post
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const getPost = async (req, res, next) => {
  try {
    const data = await PostService.getPost(req.params.id);
    if (data) {
      res.status(HttpStatus.OK).json({
        code: HttpStatus.OK,
        data: data,
        message: 'Post fetched successfully'
      });
    } else {
      res.status(HttpStatus.NOT_FOUND).json({
        code: HttpStatus.NOT_FOUND,
        message: 'Post does not exist'
      });
    }
  } catch (error) {
    next(error);
  }
}; 

/**
 * Controller to create a new Post
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const newPost = async (req, res, next) => {
  try {
    const data = await PostService.newPost(req.body);
    res.status(HttpStatus.CREATED).json({
      code: HttpStatus.CREATED,
      data: data,
      message: 'Post created successfully'
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Controller to update a Post
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const updatePost = async (req, res, next) => {
  try {
    const data = await PostService.updatePost(req.params.id, req.body);
    res.status(HttpStatus.ACCEPTED).json({
      code: HttpStatus.ACCEPTED,
      data: data,
      message: 'Post updated successfully'
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Controller to delete a single Post
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const deletePost = async (req, res, next) => {
  try {
    await PostService.deletePost(req.params.id);
    res.status(HttpStatus.OK).json({
      code: HttpStatus.OK,
      data: [],
      message: 'Post deleted successfully'
    });
  } catch (error) {
    next(error);
  }
};
