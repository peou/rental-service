import HttpStatus from 'http-status-codes';
import * as CustomerService from '../services/customer.service';

/**
 * Controller to get all customers available
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */

export const getAllCustomers = async (req, res, next) => {
  try {
    const data = await CustomerService.getAllCustomers();
    let filteredCustomers = data;
    const { page = 1, size = 10 } = req.query;
    const pageNum = parseInt(page, 10);
    const pageSize = parseInt(size, 10);
    const startIndex = (pageNum - 1) * pageSize;
    const endIndex = startIndex + pageSize;

    const paginatedCustomers = filteredCustomers.slice(startIndex, endIndex);

    const totalCustomers = filteredCustomers.length;
    const totalPages = Math.ceil(totalCustomers / pageSize);

    res.status(HttpStatus.OK).json({
      code: HttpStatus.OK,
      page: pageNum,
      size: pageSize,
      totalPages,
      totalCustomers,
      data: paginatedCustomers,
      message: 'All customers fetched successfully'
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Controller to get a single customer
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const getCustomer = async (req, res, next) => {
  try {
    const customer = await CustomerService.getCustomer(req.params.id);
    if (customer) {
      res.status(HttpStatus.OK).json({
        code: HttpStatus.OK,
        customer: customer,
        message: 'Customer fetched successfully'
      });
    } else {
      res.status(HttpStatus.NOT_FOUND).json({
        code: HttpStatus.NOT_FOUND,
        message: 'Customer does not exist'
      });
    }
  } catch (error) {
    next(error);
  }
};

/**
 * Controller to create a new customer
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const newCustomer = async (req, res, next) => {
  try {
    const data = await CustomerService.createCustomer(req.body);
    res.status(HttpStatus.CREATED).json({
      code: HttpStatus.CREATED,
      data: data,
      message: 'Customer created successfully'
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Controller to update a customer
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const updateCustomer = async (req, res, next) => {
  try {
    const data = await CustomerService.updateCustomer(req.params.id, req.body);
    res.status(HttpStatus.ACCEPTED).json({
      code: HttpStatus.ACCEPTED,
      data: data,
      message: 'Customer updated successfully'
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Controller to delete a single customer
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const deleteCustomer = async (req, res, next) => {
  try {
    await CustomerService.deleteCustomer(req.params.id);
    res.status(HttpStatus.OK).json({
      code: HttpStatus.OK,
      data: [],
      message: 'Customer deleted successfully'
    });
  } catch (error) {
    next(error);
  }
};
