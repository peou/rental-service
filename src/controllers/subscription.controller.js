import HttpStatus from 'http-status-codes';
import * as SubscriptionService from '../services/subscription.service';

/**
 * Controller to get all subscriptions available
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const getAllSubscriptions = async (req, res, next) => {
  try {
    const data = await SubscriptionService.getAllSubscriptions();
    let filteredSubscriptions = data;
    const { page = 1, size = 10 } = req.query;
    const pageNum = parseInt(page, 10);
    const pageSize = parseInt(size, 10);
    const startIndex = (pageNum - 1) * pageSize;
    const endIndex = startIndex + pageSize;

    const paginatedSubscriptions = filteredSubscriptions.slice(startIndex, endIndex);

    const totalSubscriptions = filteredSubscriptions.length;
    const totalPages = Math.ceil(totalSubscriptions / pageSize);

    res.status(HttpStatus.OK).json({
      code: HttpStatus.OK,
      page: pageNum,
      size: pageSize,
      totalPages,
      totalSubscriptions,
      data: paginatedSubscriptions,
      message: 'All subscriptions are fetched successfully'
    });
  } catch (error) {
    next(error);
  }
};


/**
 * Controller to get a single subscription
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const getSubscription = async (req, res, next) => {
  try {
    const data = await SubscriptionService.getSubscription(req.params.id);
    if (data) {
      res.status(HttpStatus.OK).json({
        code: HttpStatus.OK,
        data: data,
        message: 'Subscription fetched successfully'
      });
    } else {
      res.status(HttpStatus.NOT_FOUND).json({
        code: HttpStatus.NOT_FOUND,
        message: 'Subscription does not exist'
      });
    }
  } catch (error) {
    next(error);
  }
};

/**
 * Controller to create a new subscription
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const newSubscription = async (req, res, next) => {
  try {
    const data = await SubscriptionService.createSubscription(req.body);
    res.status(HttpStatus.CREATED).json({
      code: HttpStatus.CREATED,
      data: data,
      message: 'subscription created successfully'
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Controller to update a subscription
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const updateSubscription = async (req, res, next) => {
  try {
    const data = await SubscriptionService.updateSubscription(req.params.id, req.body);
    res.status(HttpStatus.ACCEPTED).json({
      code: HttpStatus.ACCEPTED,
      data: data,
      message: 'Subscription updated successfully'
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Controller to delete a single subscription
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const deleteSubscription = async (req, res, next) => {
  try {
    await SubscriptionService.deleteSubscription(req.params.id);
    res.status(HttpStatus.OK).json({
      code: HttpStatus.OK,
      data: [],
      message: 'Subscription deleted successfully'
    });
  } catch (error) {
    next(error);
  }
};
