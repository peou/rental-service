import HttpStatus from 'http-status-codes';
import * as userSubscriptionService from '../services/userSubscription.service';

/**
 * Controller to get all userSubscriptions available
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const getAlluserSubscriptions = async (req, res, next) => {
  try {
    const data = await userSubscriptionService.getAlluserSubscriptions();
    let filteredUserSubscriptions = data;
    const { page = 1, size = 10 } = req.query;
    const pageNum = parseInt(page, 10);
    const pageSize = parseInt(size, 10);
    const startIndex = (pageNum - 1) * pageSize;
    const endIndex = startIndex + pageSize;

    const paginatedUserSubscriptions = filteredUserSubscriptions.slice(startIndex, endIndex);

    const totalUserSubscriptions = filteredUserSubscriptions.length;
    const totalPages = Math.ceil(totalUserSubscriptions / pageSize);

    res.status(HttpStatus.OK).json({
      code: HttpStatus.OK,
      page: pageNum,
      size: pageSize,
      totalPages,
      totalUserSubscriptions,
      data: paginatedUserSubscriptions,
      message: 'All UserSubscriptions are fetched successfully'
    });
  } catch (error) {
    next(error);
  }
};


/**
 * Controller to get a single userSubscription
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const getUserSubscription = async (req, res, next) => {
  try {
    const data = await userSubscriptionService.getUserSubscription(req.params.id);
    if (data) {
      res.status(HttpStatus.OK).json({
        code: HttpStatus.OK,
        data: data,
        message: 'UserSubscription fetched successfully'
      });
    } else {
      res.status(HttpStatus.NOT_FOUND).json({
        code: HttpStatus.NOT_FOUND,
        message: 'UserSubscription does not exist'
      });
    }
  } catch (error) {
    next(error);
  }
};

/**
 * Controller to create a new user subscription
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const newUserSubscription = async (req, res, next) => {
  try {
    const data = await userSubscriptionService.createUserSubscription(req.body);
    res.status(HttpStatus.CREATED).json({
      code: HttpStatus.CREATED,
      data: data,
      message: 'user subscription created successfully'
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Controller to update a User subscription
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const updateUserSubscription = async (req, res, next) => {
  try {
    const data = await userSubscriptionService.updateUserSubscription(req.params.id, req.body);
    res.status(HttpStatus.ACCEPTED).json({
      code: HttpStatus.ACCEPTED,
      data: data,
      message: 'User Subscription updated successfully'
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Controller to delete a single user subscription
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const deleteUserSubscription = async (req, res, next) => {
  try {
    await userSubscriptionService.deleteUserSubscription(req.params.id);
    res.status(HttpStatus.OK).json({
      code: HttpStatus.OK,
      data: [],
      message: 'UserSubscription deleted successfully'
    });
  } catch (error) {
    next(error);
  }
};
