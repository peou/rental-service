import HttpStatus from 'http-status-codes';
import * as OrderService from '../services/order.service';

/**
 * Controller to get all orders available
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */

export const getAllOrders = async (req, res, next) => {
  try {
    const data = await OrderService.getAllOrders();
    let filteredOrders = data;
    const { page = 1, size = 10 } = req.query;
    const pageNum = parseInt(page, 10);
    const pageSize = parseInt(size, 10);
    const startIndex = (pageNum - 1) * pageSize;
    const endIndex = startIndex + pageSize;

    const paginatedOrders = filteredOrders.slice(startIndex, endIndex);

    const totalOrders = filteredOrders.length;
    const totalPages = Math.ceil(totalOrders / pageSize);

    res.status(HttpStatus.OK).json({
      code: HttpStatus.OK,
      page: pageNum,
      size: pageSize,
      totalPages,
      totalOrders,
      data: paginatedOrders,
      message: 'All orders fetched successfully'
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Controller to get a single role
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const getOrderBySearch = async (req, res, next) => {
  try {
    const { value } = req.query; // Assuming you're searching by name
    const orderData = await OrderService.getOrderBySearch(value);
    let filteredArticles = orderData;
    const { page = 1, size = 10 } = req.query;
    const pageNum = parseInt(page, 10);
    const pageSize = parseInt(size, 10);
    const startIndex = (pageNum - 1) * pageSize;
    const endIndex = startIndex + pageSize;

    // Get the subset of articles for the current page
    const paginatedArticles = filteredArticles.slice(startIndex, endIndex);

    // Prepare the pagination response
    const totalArticles = filteredArticles.length;
    const totalPages = Math.ceil(totalArticles / pageSize);
    
    if(totalArticles==0){
      res.status(HttpStatus.OK).json({
        code: HttpStatus.OK,
        message: 'Searching not found'
      });
    }else{
      res.status(HttpStatus.OK).json({
        code: HttpStatus.OK,
        page: pageNum,
        size: pageSize,
        totalPages,
        totalArticles,
        data: paginatedArticles,
        message: 'searching successfully'
      });
    }
  } catch (error) {
    // If an error occurs, pass it to the error handling middleware
    next(error);
  }
}

/**
 * Controller to get a single order
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const getOrder = async (req, res, next) => {
  try {
    const order = await OrderService.getOrder(req.params.id);
    if (order) {
      res.status(HttpStatus.OK).json({
        code: HttpStatus.OK,
        order: order,
        message: 'Order fetched successfully'
      });
    } else {
      res.status(HttpStatus.NOT_FOUND).json({
        code: HttpStatus.NOT_FOUND,
        message: 'Order does not exist'
      });
    }
  } catch (error) {
    next(error);
  }
};

/**
 * Controller to create a new order
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const newOrder = async (req, res, next) => {
  try {
    const data = await OrderService.createOrder(req.body);
    res.status(HttpStatus.CREATED).json({
      code: HttpStatus.CREATED,
      data: data,
      message: 'Order created successfully'
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Controller to update a order
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const updateOrder = async (req, res, next) => {
  try {
    const data = await OrderService.updateOrder(req.params.id, req.body);
    res.status(HttpStatus.ACCEPTED).json({
      code: HttpStatus.ACCEPTED,
      data: data,
      message: 'Order updated successfully'
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Controller to delete a single order
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const deleteOrder = async (req, res, next) => {
  try {
    await OrderService.deleteOrder(req.params.id);
    res.status(HttpStatus.OK).json({
      code: HttpStatus.OK,
      data: [],
      message: 'Orderer deleted successfully'
    });
  } catch (error) {
    next(error);
  }
};
