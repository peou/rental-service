import Joi from '@hapi/joi';

export const newUserValidator = (req, res, next) => {
  const schema = Joi.object({
    name: Joi.string().min(3).required(),
    email: Joi.string().email().required(), // Added email validation
    password: Joi.string().min(6).required(), // Adjusted min length for password
    phone_number: Joi.string().min(9).required(), // Adjusted min length for phone number
    address: Joi.string().min(3).required()
  });
  
  const { error, value } = schema.validate(req.body);
  
  if (error) {
    next(error);
  } else {
    req.validatedBody = value;
    next();
  }
};

export const loginValidator = (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string().email().required(), // Added email validation
    password: Joi.string().min(6).required(), // Adjusted min length for password
  });
  
  const { error, value } = schema.validate(req.body);
  
  if (error) {
    next(error);
  } else {
    req.validatedBody = value;
    next();
  }
};