import Joi from '@hapi/joi';
import { unit } from '@hapi/joi/lib/base';

export const newOrderValidator = (req, res, next) => {
  const schema = Joi.object({
    status: Joi.string().required(),
    customer_id: Joi.number().required(),
    orderItems: Joi.array().items(({
      unit_price: Joi.number().precision(2).required(),
      duration: Joi.number().required(),
      total_price: Joi.number().precision(2).required(),
    })).required(),
  });
  const { error, value } = schema.validate(req.body);
  if (error) {
    next(error);
  } else {
    req.validatedBody = value;
    next();
  }
};
