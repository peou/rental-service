import Joi from '@hapi/joi';

export const newUserSubscriptionValidator = (req, res, next) => {
  const schema = Joi.object({
    subscription_id: Joi.number().min(1).required(),
    user_id: Joi.number().min(1).required(),
    price: Joi.number().precision(2).required(),
    start_date: Joi.date().required(),
    end_date: Joi.date().required(),
  });
  const { error, value } = schema.validate(req.body);
  if (error) {
    res.status(400).json({
      code: 400,
      message: "Validation error",
      details: error.details
    });
  } else {
    req.validatedBody = value;
    next();
  }
};