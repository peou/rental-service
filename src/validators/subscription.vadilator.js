import Joi from '@hapi/joi';

export const newSubscriptionValidator = (req, res, next) => {
  const schema = Joi.object({
    name: Joi.string().min(5).required(),
    price: Joi.number().required(),
    duration: Joi.number().min(1).required(),
    number_of_posts: Joi.number().min(1).required()
  });
  const { error, value } = schema.validate(req.body);
  if (error) {
    res.status(400).json({
      code: 400,
      message: "Validation error",
      details: error.details
    });
  } else {
    req.validatedBody = value;
    next();
  }
};