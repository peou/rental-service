import Joi from '@hapi/joi';

export const newPostValidator = (req, res, next) => {
  const schema = Joi.object({
    name: Joi.string().min(3).required(),
    status: Joi.string().min(3).required(),
    user_id: Joi.number().required()
  });
  
  const { error, value } = schema.validate(req.body);
  
  if (error) {
    next(error);
  } else {
    req.validatedBody = value;
    next();
  }
};
