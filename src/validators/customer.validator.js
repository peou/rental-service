import Joi from '@hapi/joi';

export const newCustomerValidator = (req, res, next) => {
  const schema = Joi.object({
    first_name: Joi.string().min(3).required(),
    last_name: Joi.string().min(3).required(),
    email: Joi.string().min(3).required(),
    address: Joi.string().min(3).required(),
    phone_number: Joi.string().min(3).required(),
  });
  const { error, value } = schema.validate(req.body);
  if (error) {
    next(error);
  } else {
    req.validatedBody = value;
    next();
  }
};
