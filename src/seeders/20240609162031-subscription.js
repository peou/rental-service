'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {

    await queryInterface.bulkInsert('subscriptions', [
      {
        id: 1,
        name: "monthly",
        duration: 1,
        number_of_posts: 12,
        price: 12,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: 2,
        name: "three months",
        duration: 3,
        number_of_posts: 40,
        price: 30,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: 3,
        name: "6 months",
        duration: 6,
        number_of_posts: 80,
        price: 50,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: 4,
        name: "yearly",
        duration: 12,
        number_of_posts: 200,
        price: 100,
        created_at: new Date(),
        updated_at: new Date()
      }
    ])
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('subscriptions', null, {});
  }
};
