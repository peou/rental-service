'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {

    await queryInterface.bulkInsert('customers', [
      {
        id: 1,
        first_name: "Dan",
        last_name: "Niel",
        address: "Seoul, South Korea",
        phone_number: "012345678",
        email: "dan.niel6465655@jgjjg.co",
        created_at: new Date(),
        updated_at: new Date()
      }, {
        id: 2,
        first_name: "Lee",
        last_name: "Dong Wook",
        address: "Seoul, South Korea",
        phone_number: "012366678",
        email: "lee.ookieieh65678@jgjjg.co",
        created_at: new Date(),
        updated_at: new Date()
      }, {
        id: 3,
        first_name: "Song",
        last_name: "JoongKi",
        address: "Seoul, South Korea",
        phone_number: "013345678",
        email: "song.jongkieiiei44@jgjjg.co",
        created_at: new Date(),
        updated_at: new Date()
      },])
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('customers', null, {});
  }
};
