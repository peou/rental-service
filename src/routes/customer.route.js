import express from 'express';
import * as customerController from '../controllers/customer.controller';
import { newCustomerValidator } from '../validators/customer.validator';

const router = express.Router();

//route to get all customers
router.get('', customerController.getAllCustomers);

//route to create a new customer
router.post('', newCustomerValidator, customerController.newCustomer);

//route to get a single customer by their customer id
router.get('/:id', customerController.getCustomer);

//route to update a single customer by their customer id
router.put('/:id', customerController.updateCustomer);

//route to delete a single customer by their customer id
router.delete('/:id', customerController.deleteCustomer);

export default router;
