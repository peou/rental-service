import express from 'express';
import * as subscriptionController from '../controllers/subscription.controller';
import { newSubscriptionValidator } from '../validators/subscription.vadilator';
import { userAuth } from '../middlewares/auth.middleware';

const router = express.Router();

router.get('', subscriptionController.getAllSubscriptions);
router.post('', newSubscriptionValidator, subscriptionController.newSubscription);
router.get('/:id', subscriptionController.getSubscription);
router.put('/:id', subscriptionController.updateSubscription);
router.delete('/:id', subscriptionController.deleteSubscription);

export default router;
