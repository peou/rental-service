import express from 'express';
import * as orderController from '../controllers/order.controller';
import { newOrderValidator } from '../validators/order.validator';

const router = express.Router();

//route to get all posts
router.get('', orderController.getAllOrders);

//route to search order
router.get('/search', orderController.getOrderBySearch);

//route to get a single post by their post id
router.get('/:id', orderController.getOrder);

//route to create a new post
router.post('', newOrderValidator, orderController.newOrder);

//route to update a single post by their post id
router.put('/:id', newOrderValidator, orderController.updateOrder);

//route to delete a single post by their post id
router.delete('/:id', orderController.deleteOrder);

export default router;
