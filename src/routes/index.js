import express from 'express';
const router = express.Router();

import customerRoute from './customer.route';
import postRoute from './post.route';
import roleRoute from './role.route';
import subscriptionRoute from './subscription.route';
import userSubscriptionRoute from './userSubscription.route';
import userRoute from './user.route';
import orderRoute from './order.route';
import { userAuth } from '../middlewares/auth.middleware';

/**
 * Function contains Application routes
 *
 * @returns router
 */
const routes = () => {
  router.get('/', (req, res) => {
    res.json('Welcome');
  });
  router.use('/customers', customerRoute);
  router.use('/posts',userAuth, postRoute);
  router.use('/roles',userAuth, roleRoute);
  router.use('/subscriptions',userAuth, subscriptionRoute);
  router.use('/userSubscriptions',userAuth, userSubscriptionRoute);
  router.use('/users', userRoute);
  router.use('/orders', orderRoute);

  return router;
};

export default routes;
