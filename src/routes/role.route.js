import express from 'express';
import * as roleController from '../controllers/role.controller';
import { newRoleValidator } from '../validators/role.validator';
import { userAuth } from '../middlewares/auth.middleware';

const router = express.Router();

//route to get all roles
router.get('', roleController.getAllRoles);

//route to search role
router.get('/search', roleController.getRoleBySearch);

//route to get a single role by their role id
router.get('/:id', roleController.getRole);

//route to create a new role
router.post('', newRoleValidator, roleController.newRole);

//route to update a single role by their role id
router.put('/:id', newRoleValidator, roleController.updateRole);

//route to delete a single role by their role id
router.delete('/:id', roleController.deleteRole);

export default router;
