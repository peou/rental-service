import express from 'express';
import * as postController from '../controllers/post.controller';
import { newPostValidator } from '../validators/post.validator';

const router = express.Router();

//route to get all posts
router.get('', postController.getAllPosts);

//route to search post
router.get('/search', postController.getPostBySearch);

//route to get a single post by their post id
router.get('/:id', postController.getPost);

//route to create a new post
router.post('', [newPostValidator], postController.newPost);

//route to update a single post by their post id
router.put('/:id', [newPostValidator], postController.updatePost);

//route to delete a single post by their post id
router.delete('/:id', postController.deletePost);

export default router;
