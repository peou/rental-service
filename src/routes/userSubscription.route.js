import express from 'express';
import * as userSubscriptionController from '../controllers/userSubscription.controller';
import { newUserSubscriptionValidator } from '../validators/userSubscription.validator';
import { userAuth } from '../middlewares/auth.middleware';

const router = express.Router();

router.get('',userAuth, userSubscriptionController.getAlluserSubscriptions);
router.post('', userAuth,newUserSubscriptionValidator, userSubscriptionController.newUserSubscription);
router.get('/:id', userAuth,userSubscriptionController.getUserSubscription);
router.put('/:id', userAuth,userSubscriptionController.updateUserSubscription);
router.delete('/:id', userAuth,userSubscriptionController.deleteUserSubscription);

export default router;
