import DB from '../models/index';
const { Op } = require('sequelize');

//get all Posts
export const getAllPosts = async () => {
  const data = await DB.Post.findAll();
  return data;
};

//create new Post
export const newPost = async (body) => {
  const data = await DB.Post.create(body);
  return data;
};

//update single Post
export const updatePost = async (id, body) => {
  await DB.Post.update(body, {
    where: { id: id }
  });
  return body;
};

//delete single Post
export const deletePost = async (id) => {
  await DB.Post.destroy({ where: { id: id } });
  return '';
};

//get single Post
export const getPost = async (id) => {
  const data = await DB.Post.findByPk(id);
  return data;
};

export const getPostBySearch = async (value) => {
  
  const posts = await DB.Post.findAll({
    where: {
        [Op.or]: [
          { name: { [Op.like]: `%${value}%` } },
          { status: { [Op.like]: `%${value}%` } }
        ]
    }
  });
  return posts;
};
