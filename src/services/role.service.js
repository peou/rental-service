import DB from '../models/index';
const { Op } = require('sequelize');

//get all roles
export const getAllRoles = async () => {
  const data = await DB.Role.findAll();
  return data;
};

//create new role
export const newRole = async (body) => {
  const data = await DB.Role.create(body);
  return data;
};

//update single role
export const updateRole = async (id, body) => {
  await DB.Role.update(body, {
    where: { id: id }
  });
  return body;
};

//delete single role
export const deleteRole = async (id) => {
  await DB.Role.destroy({ where: { id: id } });
  return '';
};

//get single role
export const getRole = async (id) => {
  const data = await DB.Role.findByPk(id);
  return data;
};


export const getRoleBySearch = async (value) => {
  
  const roles = await DB.Role.findAll({
    where: {
      slug: {
        [Op.like]: `%${value}%`
      }
    }
  });
  return roles;
};
