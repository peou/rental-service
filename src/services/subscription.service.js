import DB from '../models/index';

export const getAllSubscriptions = async () => {
  const data = await DB.Subscription.findAll();
  return data;
};

export const createSubscription = async (body) => {
  const data = await DB.Subscription.create(body);
  return data;
};

export const updateSubscription = async (id, body) => {
  await DB.Subscription.update(body, {
    where: { id: id }
  });
  return body;
};

export const deleteSubscription = async (id) => {
  await DB.Subscription.destroy({ where: { id: id } });
  return '';
};

export const getSubscription = async (id) => {
  const data = await DB.Subscription.findByPk(id);
  return data;
};
