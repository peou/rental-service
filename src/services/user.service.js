import * as bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import DB from '../models/index';

import dotenv from 'dotenv';
dotenv.config();

const excludeDatas = ['passwordhash', 'passwordsalt']
const { Op } = require('sequelize');

//get all User
export const getAllUsers = async () => {
  const data = await DB.User.findAll({
    include: {
      model: DB.Role,
      as: 'role'
    },
    attributes: { exclude: excludeDatas }
  });
  return data;
};

//create new User
export const newUser = async (body) => {
  const passwordsalt = bcrypt.genSaltSync (10)
  const passwordhash = bcrypt.hashSync(body.password, passwordsalt)

  body.passwordsalt = passwordsalt
  body.passwordhash = passwordhash
  delete body.password

  const data = await DB.User.create(body, {
    include: {
      model: DB.Role,
      as: 'role'
    },
    attributes: { exclude: excludeDatas }
  });
  return data;
};

//update single User
export const updateUser = async (id, body) => {
  await DB.User.update(body, {
    where: { id: id },
    attributes: { exclude: excludeDatas }
  });
  return await getUser(id);
};

//delete single User
export const deleteUser = async (id) => {
  await DB.User.destroy({ where: { id: id } });
  return '';
};

//get single User
export const getUser = async (id) => {
  const data = await DB.User.findByPk(id, {
    include: {
      model: DB.Role,
      as: 'role'
    },
    attributes: { exclude: excludeDatas }
  });
  return data;
};

export const login = async (body) => {
  const user = await DB.User.findOne({
    where: { email: body.email },
  });

  switch (true) {

    case !user:
      throw new Error('User not found')

    case !bcrypt.compareSync(body.password, user.passwordhash):
      throw new Error('Password is wrong')

    default:

      const jwt_token = jwt.sign({ 
        id: user.dataValues.id,
        email: user.dataValues.email,
        name: user.dataValues.name,
        role: user.dataValues.role_id
      }, process.env.JWT_SECRET)

      user.dataValues.jwt_token = jwt_token
      return await updateUser(user.dataValues.id, user.dataValues)
  }
}
export const getUserBySearch = async (value) => {
  
  const users = await DB.User.findAll({
    where: {
        [Op.or]: [
          { name: { [Op.like]: `%${value}%` } },
          { address: { [Op.like]: `%${value}%` } },
          { phone_number: { [Op.like]: `%${value}%` } },
          { email: { [Op.like]: `%${value}%` } }
        ]
    }
  });
  return users;
};
