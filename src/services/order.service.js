import { model } from 'mongoose';
import DB from '../models/index';
const { Op } = require('sequelize');
//get all orders
export const getAllOrders = async () => {
  const data = await DB.Order.findAll({
    include: {
      model: DB.OrderItem,
      as: 'orderItems'
    }
  });
  return data;
};

//create new order
export const createOrder = async (body) => {
  body['grand_total'] = body.orderItems.reduce((acc, item) => acc + item.total_price, 0);
  const data = await DB.Order.create(body, {
    include: {
      model: DB.OrderItem,
      as: 'orderItems'
    }
  });
  return data;
};

//update single order
export const updateOrder = async (id, body) => {
  await DB.Order.update(body, {
    where: { id: id }
  });
  return body;
};

//delete single order
export const deleteOrder = async (id) => {
  await DB.Order.destroy({ where: { id: id } });
  return '';
};

//get single order
export const getOrder = async (id) => {
  const data = await DB.Order.findByPk(id, {
    include: {
      model: DB.OrderItem,
      as: 'orderItems'
    }
  });
  return data;
};

export const getOrderBySearch = async (value) => {
  
  const orders = await DB.Order.findAll({
    where: {
      status: {
        [Op.like]: `%${value}%`
      }
    },
    include: {
      model: DB.OrderItem,
      as: 'orderItems'
    }
  });
  return orders;
};
