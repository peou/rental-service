import DB from '../models/index';

//get all customers
export const getAllCustomers = async () => {
  const data = await DB.Customer.findAll();
  return data;
};

//create new customer
export const createCustomer = async (body) => {
  const data = await DB.Customer.create(body);
  return data;
};

//update single customer
export const updateCustomer = async (id, body) => {
  await DB.Customer.update(body, {
    where: { id: id }
  });
  return body;
};

//delete single customer
export const deleteCustomer = async (id) => {
  await DB.Customer.destroy({ where: { id: id } });
  return '';
};

//get single customer
export const getCustomer = async (id) => {
  const data = await DB.Customer.findByPk(id);
  return data;
};
