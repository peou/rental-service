import DB from '../models/index';

export const getAlluserSubscriptions = async () => {
  const data = await DB.userSubscription.findAll();
  return data;
};

export const createUserSubscription = async (body) => {
  const data = await DB.userSubscription.create(body);
  return data;
};

export const updateUserSubscription = async (id, body) => {
  await DB.userSubscription.update(body, {
    where: { id: id }
  });
  return body;
};

export const deleteUserSubscription = async (id) => {
  await DB.userSubscription.destroy({ where: { id: id } });
  return '';
};

export const getUserSubscription = async (id) => {
  const data = await DB.userSubscription.findByPk(id);
  return data;
};
