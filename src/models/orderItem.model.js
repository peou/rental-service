'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class OrderItem extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      OrderItem.belongsTo(models.Post, {
        foreignKey: 'post_id'
      })
      OrderItem.belongsTo(models.Order, {
        foreignKey: 'order_id'
      })
    }
  }
  OrderItem.init(
    {
      unit_price: DataTypes.DOUBLE,
      duration: DataTypes.INTEGER,
      total_price: DataTypes.DOUBLE,
      created_at: DataTypes.DATE,
      updated_at: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: 'OrderItem',
      tableName: 'orderItems',
      timestamps: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at'
    }
  );
  return OrderItem;
};
