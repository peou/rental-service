'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.hasMany(models.Post, {
        as: 'posts',
        foreignKey: 'user_id'
      })
      User.hasMany(models.userSubscription, {
        as: 'userSubscriptions',
        foreignKey: 'user_id'
      })
      User.belongsTo(models.Role, {
        as: 'role',
        foreignKey: 'id'
      })
    }
  }
  User.init(
    {
      name: DataTypes.STRING,
      address: DataTypes.TEXT,
      passwordsalt: DataTypes.STRING,
      passwordhash: DataTypes.STRING,
      jwt_token: DataTypes.TEXT,

      phone_number: {
        type: DataTypes.STRING,
        validate: { 
          isNumeric: true,
          len: [9, 10]
        }
      },
      
      email: {
        type: DataTypes.STRING,
        validate: { isEmail: true },
        unique: true
      },
      
      role_id: DataTypes.INTEGER,
      created_at: DataTypes.DATE,
      updated_at: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: 'User',
      tableName: 'users',
      timestamps: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at'
    }
  );
  return User;
};
