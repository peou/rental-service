'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Customer extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Customer.hasMany(models.Order, {
        as: 'orders',
        foreignKey: 'customer_id'
      })
    }
  }
  Customer.init(
    {
      first_name: DataTypes.STRING,
      last_name: DataTypes.STRING,
      address: DataTypes.TEXT,
      phone_number: {
        type: DataTypes.STRING,
        validate: { 
          isNumeric: true,
          len: [1, 10]
        }
      },
      email: {
        type: DataTypes.STRING,
        validate: { isEmail: true }
      },
      created_at: DataTypes.DATE,
      updated_at: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: 'Customer',
      tableName: 'customers',
      timestamps: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at'
    }
  );
  return Customer;
};
