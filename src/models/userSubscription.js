'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserSubsciption extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      UserSubsciption.belongsTo(models.Subscription, {
        foreignKey: 'subscription_id'
      })
      UserSubsciption.belongsTo(models.User, {
        foreignKey: 'user_id'
      })
    }
  }
  UserSubsciption.init(
    {
      subscription_id: DataTypes.INTEGER,
      user_id: DataTypes.INTEGER,
      price: DataTypes.DOUBLE,
      start_date: DataTypes.DATE,
      end_date: DataTypes.DATE,
      created_at: DataTypes.DATE,
      updated_at: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: 'userSubscription',
      tableName: 'userSubscriptions',
      timestamps: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at'
    }
  );
  return UserSubsciption;
};
