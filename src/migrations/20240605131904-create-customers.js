'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
  
    await queryInterface.createTable('customers', {

      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },

      email: {
        type: Sequelize.STRING,
        validate: { isEmail: true }
      },

      first_name: Sequelize.STRING,
      last_name: Sequelize.STRING,
      phone_number: Sequelize.STRING,
      address: Sequelize.TEXT,

      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE
      }

    });

  },

  async down (queryInterface, Sequelize) {
    await queryInterface.dropTable('customers');
  }
};
