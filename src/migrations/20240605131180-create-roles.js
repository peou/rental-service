'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    
    await queryInterface.createTable('roles', {

      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },

      slug: Sequelize.STRING,

      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE
      }

    });

  },

  async down (queryInterface, Sequelize) {
    await queryInterface.dropTable('roles');
  }
};
