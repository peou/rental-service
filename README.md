## Installation

Install project's dependencies

```bash
npm install
```

Build docker image

```bash
docker-compose --build
```

Launch the project in the background

```bash
docker-compose up -d
```

## Access to Container

```bash
docker exec -it rental_project_api bash
```

## Run migration

Follow the step 'Access to Container'

```bash
npx sequelize db:migrate
```

## Run seeder

Follow the step 'Access to Container'

```bash
npx sequelize db:seed:all
```
