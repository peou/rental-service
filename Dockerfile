FROM node:18.16

WORKDIR /home/node/app

# Copy package.json and package-lock.json
COPY package*.json ./

# Install the dependencies
RUN npm install

# Copy the rest of the application code
COPY . .

# Expose the port the app runs on
EXPOSE 3000

# Command to run the application
# CMD ["node", "app.js"]
CMD ["npm", "run", "dev"]